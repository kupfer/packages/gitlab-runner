# Gitlab Runner

This doc describes how to setup the Gitlab Runner instances used for building packages (aka. Packaging CI), testing Kupferboostrap, etc.

Currently, the runner needs to be x86_64 as we don't have multi-arch Kupferbootstrap Docker images yet.

Any common Linux distro that can run Docker and the `gitlab-runner` Go binary should work as the host OS.<br> We use Debian and Arch Linux btw.

For **Contributors** developing Kupfer patches/packages, you can use your linux workstation/laptop for the runner too.

The runner will need to be run with docker privileged mode, as kupferbootstrap needs to be able to bind-mount. The configuration section covers this.

[TOC]

## Setting up any Runner

**The commands in this tutorial are assumed to be run as root.**

### Installing Gitlab Runner

1. Install the docker daemon, e.g. for Debian: `apt install docker.io`
1. Install a gitlab runner: https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner

### Registering the Runner with Gitlab

Any Runner daemon can be registered with any number of Gitlab projects any number of times and be configured differently in each instance, appearing as a different Runner to Gitlab each.
<br>**Tl;dr: Any Runner daemon can host an infinite number of individual "runners".**

Let's register an instance for your Kupfer project:

1. Navigate to the repository or group you wish to use the runner for (usually `pkgbuilds`) on Gitlab.
2. In the Sidebar click on: Setttings => CI/CD
3. expand the "Runners" section
4. You will see a **token** for registering new runners. Copy it.
5. On your Runner host, run `gitlab-runner register`.
   <br>It will prompt you for a handful of things:
   1. The **domain** of the Gitlab instance. Usually `gitlab.com`.
   2. The **registration token** you just copied.
   3. An **executor**: Choose `docker`.
   4. A **default image**. Your choice here doesn't really matter as all our CI configs specify an image. When in doubt, leave it at the default or use `archlinux`.
   5. A **name**. Anything you want, pick something that lets you know which machine it is in a list of Runners.
   6. **Tags**: These allow the CI scripts to choose which runner to run on.
      We usually add at least `docker`.<br>
      For the packaging CI we use `docker, packages`<br>
      **These can later be edited through the Gitlab UI by clicking the pencil icon next to the runner in the Gitlab CI/CD Settings => "Runners" Section.**
6. Completing this dialogue will create an entry in the `/etc/gitlab-runner/config.toml` file for you.

## Configuring the Runner daemon

We'll need to make some necessary config changes first, mostly enabling **privileged** mode.

1. Edit your `/etc/gitlab-runner/config.toml`.
2. It should look something like this:
   ```toml
   concurrent = 6
   check_interval = 0

   [session_server]
   session_timeout = 1800

   [[runners]]
      name = "[the name you entered]"
      url = "https://gitlab.com/"
      token = "[a token]"
      executor = "docker"
      [runners.cache]
         ...
      [runners.docker]
         ...
   ```

If you've created a runner before, you might have multiple ``[[runners]]`` sections, each describing a runner instance. New instances are usually appended at the end of the file.

### Edit global settings

We recommend changing the global settings at the top of the file as follows:
```toml
concurrent = 4
check_interval = 10
```

### Edit runner instance settings

 In your runner instance's `[[runners]]` block, add or change `privileged` to `true`:

 ```toml
[[runners]]
   name = "foo"
   ...
   [runners.docker]
      ...
      privileged = true
```

## Starting the Runner daemon

1. The gitlab runner should automatically detect the file modification and reload the config if it's already running.
2. Make sure the gitlab-runner is running right now: `systemctl start gitlab-runner`.
   <br>Alternatively: enable it for **autostart** and start it in one command: `systemctl enable --now gitlab-runner`.
3. In the CI/CD Settings' "Runners" tab, the runner should now be listed and have a green status icon.

### Configure the job timeout (=maximum job duration) on Gitlab
1. Navigate to the CI/CD Settings page as described in steps 1-2 of [Registering the Runner with Gitlab](#registering-the-runner-with-gitlab) above.
2. Expand the "General Pipelines" section and set the maximum job timeout for the runner to 4h.

### For Contributors: Configure the runner to run untagged jobs
1. Navigate to the CI/CD Settings page as described in steps 1-2 of [Registering the Runner with Gitlab](#registering-the-runner-with-gitlab) above or stay there.
2. Expand the "Runners" section
3. Click on the pencil icon next to your runner and set the following options:
   - `Active`: checked
   - `Protected`: unchecked. Allow the runner to run for unprotected branches too.
   - `Run untagged jobs`: checked. Allow the runner to pick jobs that don't specify runner tags.

## Only for official Kupfer Packaging CI runners

The following instructions are only for the official Kupfer packaging CI.

**Contributors don't need to bother with these steps!**

These steps should be run on your local machine, as your regular user.

Note: **Make sure the runner only runs for protected branches and has the `docker` and `packages` tags!**<br>
Adapt the instructions in [Configure the runner to run untagged jobs](#for-contributors-configure-the-runner-to-run-untagged-jobs) to your needs.

### Generate SSH key
1. Generate a new ssh key with no (an empty) keyphrase.
   ```bash
   cd /tmp && ssh-keygen -t ed25519 -f kupfer_ci
   ```
2. Add the SSH key from `kupfer_ci.pub` to the `prebuilts` Gitlab project as a read+write **Deploy Key** in the project's Settings => Repository.
3. Base64 encode the private key.
   **Warning: Sensitive key material, b64 is NOT encryption!**
   ```bash
   openssl enc -base64 -A -in kupfer_ci -out kupfer_ci.base64
   ```
4. Store the content of `kupfer_ci.base64` in the `SSH_KEY` variable in the "Variables" section of the CI/CD settings of the `pkgbuilds` project.
   <br>Make sure to mark the variable as **protected and masked!**
